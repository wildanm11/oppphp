<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal("ayam");

echo "nama hewan adalah : $sheep->name <br>"; // "shaun"
echo "mempunyai kaki berjumlah : $sheep->legs <br>"; // 4
echo "merupakan jenis pemakan segalanya : $sheep->cold_blooded <br> <br>"; // "no"

$sheep1 = new Ape("kera sakti");

echo "nama hewan adalah : $sheep1->name <br>"; // "shaun"
echo "mempunyai kaki berjumlah : $sheep1->legs <br>"; // 4
echo "merupakan jenis pemakan segalanya : $sheep1->cold_blooded<br>"; // "no"
$sheep1->yellyell();

$sheep2 = new frog("buduk");

echo "nama hewan adalah : $sheep2->name <br>"; // "shaun"
echo "mempunyai kaki berjumlah : $sheep2->legs <br>"; // 4
echo "merupakan jenis pemakan segalanya : $sheep2->cold_blooded<br>"; // "no"
$sheep2->Jump()
?>




